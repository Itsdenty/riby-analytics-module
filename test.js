var analytics = require('./analytics');
var activity = {"activity":{
	// "riby_id": "RFC",
	"initiator_type": "sampleinitiatortype",
	"initiator_id": "sampleid",
	"resource_type": "sampleresourcetype",
	"resource_ids": ["samplestring"],
	"activity_type": "visit",
	// "message": "samplemessage",
	"meta": {"meta": "samplemeta"},
	"ip_address": "10.199.212.333",
	"user_agent": "libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/0.8.12",
	"route": "sampleroute",
	"status": 123,
	"module": "contribution-manager",
	"product_code": "RFC"
}}
var apiResult = analytics.create(activity, "Bearer e7bb7411ab3abd41b9db0ab3274b9170c0487daa");
console.log(apiResult);