var Promise = require('bluebird'),
    requestPromise = require('request-promise'),
    amqp = require('amqplib/callback_api'),
    url = require('./url'),
    analyticEndpoint = process.env.ANALYTICAL_URL || url.url;
var analytics = {};

analytics.create = function(activity, token="sample token"){
    amqp.connect('amqp://localhost', function(err, conn) {
        conn.createChannel(function(err, ch) {
            var q = 'activity';
            // var msg = 'Hello World!';
            activity = {activity : activity, token : token};
            var act = activity;
            act = JSON.stringify(act);
            ch.assertQueue(q, {durable: false});
            // Note: on Node 6 Buffer.from(msg) should be used
            ch.sendToQueue(q, new Buffer(act));
            console.log(" [x] Sent %s", q);
        });
        setTimeout(function() { conn.close(); process.exit(0) }, 500);
    });
}

analytics.receive = function(){
    amqp.connect('amqp://localhost', function(err, conn) {
        conn.createChannel(function(err, ch) {
          var q = 'activity';
      
          ch.assertQueue(q, {durable: false});
          console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
          ch.consume(q, function(act) {
              var receivedActivity = JSON.parse(act.content);
                  return new Promise(function(resolve, reject){
                      requestPromise({
                          method: 'POST',
                          uri: analyticEndpoint,
                          form: receivedActivity.activity,
                          headers: {
                              'Authorization': receivedActivity.token
                          },
                          json: true
                      }).then(function(parsedBody){
                          console.log(parsedBody);
                          // var parsedBody = JSON.parse(parsedBody);
                          console.log(parsedBody)
                          resolve(parsedBody);
                      }).catch(function(error){
                          // error = JSON.parse(error);
                          console.log(error);
                          reject(error);
                      });
                  });
              // _console.log(" [x] Received %s", act.content.toString());
            }, {noAck: true});
        });
    });
}
module.exports = analytics;